DROP TABLE IF EXISTS user_like_post;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS user;

CREATE TABLE user(  
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);

CREATE TABLE post (
    id INT PRIMARY KEY AUTO_INCREMENT,
    content TEXT,
    id_author INT,
    FOREIGN KEY (id_author) REFERENCES user(id)
);

CREATE TABLE user_like_post (
    id_user INT,
    id_post INT,
    PRIMARY KEY (id_user,id_post),
    FOREIGN KEY (id_user) REFERENCES user(id),
    FOREIGN KEY (id_post) REFERENCES post(id)    
);

-- Ajoute un user avec comme username test@test.com et comme password 1234
INSERT INTO user (email,password,role) VALUES ("test@test.com", "$2a$10$ekRvhHNe6oHW2UZHK3qsXuWYlV90jo42Q3iJYydpI.6OgmfMZSTVa", "ROLE_USER");

INSERT INTO post (content, id_author) VALUES ("test", 1);
INSERT INTO post (content, id_author) VALUES ("test2", 1);

INSERT INTO user_like_post (id_user,id_post) VALUES (1, 1);
INSERT INTO user_like_post (id_user,id_post) VALUES (1, 2);