package co.simplon.promo16.auth;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import co.simplon.promo16.auth.repository.UserRepository;

@ActiveProfiles("test")
@SpringBootTest
@Sql(scripts={"/schema.sql", "/data.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
class AuthApplicationTests {

	@Autowired
	private UserRepository repo;

	@Test
	void contextLoads() {
		assertEquals(1, repo.findAll().size());
	}

}
