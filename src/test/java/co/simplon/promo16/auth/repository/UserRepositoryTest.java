package co.simplon.promo16.auth.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import co.simplon.promo16.auth.entity.User;

@ActiveProfiles("test")
@SpringBootTest
@Sql(scripts={"/schema.sql", "/data.sql"}, executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    UserRepository repo;


    @Test
    void testFindAll() {
        List<User> result = repo.findAll();
        assertNotEquals(0, result.size());
        User first = result.get(0);
        assertEquals("test@test.com", first.getEmail());
        assertEquals("ROLE_ADMIN", first.getRole());
        
    }

    @Test
    void testFindByEmail() {
        User first = repo.findByEmail("test@test.com");
        assertEquals("test@test.com", first.getEmail());
        assertEquals("ROLE_ADMIN", first.getRole());

    }
    @Test
    void testFindByEmailUserNotFound() {
        User first = repo.findByEmail("blablabla");
        assertNull(first);

    }

    @Test
    void testLoadUserByUsernameShouldThrowOnUserNotFound() {
        //Test qu'une UsernameNotFoundException sera levée lorsqu'on exécutera le
        //repo.loadUserByUsername("blabla")
        assertThrows(UsernameNotFoundException.class, () -> {
            repo.loadUserByUsername("blabla");
        });
        
    }

    @Test
    void testSave() {

        User toSave = new User("test@bloup.com", "test", "ROLE_USER");
        assertTrue(repo.save(toSave));
        assertEquals(2, toSave.getId());
    }
    @Test
    void testSave2() {

        User toSave = new User("test@bloup.com", "test", "ROLE_USER");
        assertTrue(repo.save(toSave));
        assertEquals(2, toSave.getId());
    }
    @Test
    void testSave3() {

        User toSave = new User("test@bloup.com", "test", "ROLE_USER");
        assertTrue(repo.save(toSave));
        assertEquals(2, toSave.getId());
    }
    @Test
    void testUpdate() {
        User toUpdate = new User(1, "test@test.com", "test", "ROLE_USER");
        
        assertTrue(repo.update(toUpdate));
    }
}
