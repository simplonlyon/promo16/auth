
INSERT INTO user (email,password,role) VALUES ('test@test.com', '$2a$10$ekRvhHNe6oHW2UZHK3qsXuWYlV90jo42Q3iJYydpI.6OgmfMZSTVa', 'ROLE_USER');

INSERT INTO post (content, id_author) VALUES ('test', 1);
INSERT INTO post (content, id_author) VALUES ('test2', 1);

INSERT INTO user_like_post (id_user,id_post) VALUES (1, 1);
INSERT INTO user_like_post (id_user,id_post) VALUES (1, 2);