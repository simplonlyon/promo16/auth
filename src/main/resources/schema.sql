DROP TABLE IF EXISTS user_like_post;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS user;

CREATE TABLE user(  
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);

CREATE TABLE post (
    id INT PRIMARY KEY AUTO_INCREMENT,
    content TEXT,
    id_author INT,
    FOREIGN KEY (id_author) REFERENCES user(id)
);

CREATE TABLE user_like_post (
    id_user INT,
    id_post INT,
    PRIMARY KEY (id_user,id_post),
    FOREIGN KEY (id_user) REFERENCES user(id),
    FOREIGN KEY (id_post) REFERENCES post(id)    
);