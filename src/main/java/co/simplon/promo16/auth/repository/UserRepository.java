package co.simplon.promo16.auth.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.auth.entity.User;

/**
 * Ce repository nous permettra de rajouter un user en base de données, mais
 * aussi
 * de récupérer un User par son email, afin de vérifier si un email est déjà
 * pris à l'inscription
 * ou de récupérer un User dans le cas d'un login
 * On lui fait également implémenter l'interface UserDetailsService qui rajoute
 * une
 * méthode loadUserByUsername dont Spring a besoin pour récupérer un User
 */
@Repository
public class UserRepository implements UserDetailsService {
    @Autowired
    private DataSource dataSource;

    @Autowired
    @Lazy
    private PostRepository postRepo;

    private Connection connection;

    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM user");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new User(
                                result.getInt("id"),
                                result.getString("email"),
                                result.getString("password"),
                                result.getString("role")
                            )
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    public boolean update(User user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE user SET email=?,password=?,role=? WHERE id=?");

            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getRole());
            stmt.setInt(4, user.getId());
            
            return stmt.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    /**
     * Méthode qui ajoute un user en base de données
     * 
     * @param user Le user à ajouter en base de données
     * @return true si ça à marcher, false sinon
     */
    public boolean save(User user) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO user (email,password,role) VALUES (?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getRole());
            // cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return false;
    }

    /**
     * Méthode permettant de récupérer un user en se basant sur l'email
     * 
     * @param email l'email de l'user recherché
     * @return Le user correspondant à l'email ou null si pas de user
     */
    public User findByEmail(String email) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                User user = new User(
                        result.getInt("id"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
                //Version avec variable intermédiaire
                // List<Post> posts = postRepo.findByAuthor(user.getId());
                //user.setPosts(posts);
                user.setPosts(postRepo.findByAuthor(user.getId()));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    /**
     * Méthode permettant de récupérer un user en se basant sur l'id
     * 
     * @param id l'id du user recherché
     * @return Le user correspondant à l'id ou null si pas de user
     */
    public User findById(int id) {
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM user WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    /**
     * La méthode de l'interface pour Spring, au final dedans on ne fait qu'appeler
     * notre
     * méthode findByEmail (vu que dans notre cas notre email est notre username)
     * 
     * @param username Le username (ici l'email) du user recherché
     * @return Le User correspondant à l'email
     * @throws UsernameNotFoundException On fait en sorte de déclencher une
     *                                   exception si aucun User ne correspond à
     *                                   l'email donné (c'est spring qui veut ça)
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

}
