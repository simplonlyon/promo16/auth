package co.simplon.promo16.auth.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.auth.entity.Post;
import co.simplon.promo16.auth.entity.User;

@Repository
public class PostRepository {
    @Autowired
    private DataSource dataSource;
    @Autowired
    @Lazy
    private UserRepository userRepo;

    public List<Post> findByAuthor(int idAuthor) {
        List<Post> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM post WHERE id_author=?");
            stmt.setInt(1, idAuthor);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Post(
                                result.getInt("id"),
                                result.getString("content"))
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Méthode permettant de récupérer un post en se basant sur l'id
     * 
     * @param id l'id du post recherché
     * @return Le post correspondant à l'id ou null si pas de post
     */
    public Post findById(int id, boolean withAuthor) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM post WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                Post post = new Post(
                        result.getInt("id"),
                        result.getString("content"));
                if (withAuthor) {
                    User author = userRepo.findById(result.getInt("id_author"));
                    post.setAuthor(author);
                }
                return post;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return null;
    }

    public Post findById(int id) {
        return findById(id, false);
    }
}
