package co.simplon.promo16.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import co.simplon.promo16.auth.entity.User;
import co.simplon.promo16.auth.repository.UserRepository;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private UserRepository repo;
    
    @GetMapping("/manage-users") // Donc ça on y accède sur http://localhost:8080/admin/manage-users
    public String manageUser(Model model) {
        model.addAttribute("users", repo.findAll());
        return "manage-users";
    }

    @PostMapping("/promote-user/{email}")
    public String promoteUser(@PathVariable("email") String email) {
        User user = repo.findByEmail(email);
        user.setRole("ROLE_ADMIN");
        repo.update(user);
        return "redirect:/admin/manage-users";
    }
}
