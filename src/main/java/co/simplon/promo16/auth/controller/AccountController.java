package co.simplon.promo16.auth.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.auth.entity.User;

@Controller
public class AccountController {
    
    @GetMapping("/account")
    public String showAccount(Authentication authentication) {
        User user = (User)authentication.getPrincipal();
        System.out.println(user.getEmail());

        return "account";
    }
}
