package co.simplon.promo16.auth.entity;

import java.util.ArrayList;
import java.util.List;

public class Post {
    private Integer id;
    private String content;
    private User author;
    private List<User> likes = new ArrayList<>();
    public Post(String content) {
        this.content = content;
    }
    public List<User> getLikes() {
        return likes;
    }
    public void setLikes(List<User> likes) {
        this.likes = likes;
    }
    public Post() {
    }
    public Post(Integer id, String content) {
        this.id = id;
        this.content = content;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public User getAuthor() {
        return author;
    }
    public void setAuthor(User author) {
        this.author = author;
    }
}
