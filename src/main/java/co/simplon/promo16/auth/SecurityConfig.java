package co.simplon.promo16.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


import co.simplon.promo16.auth.repository.UserRepository;

/**
 * Cette classe est obligatoire pour configurer notre SpringSecurity, elle sera toujours
 * globalement similaire, sauf pour la méthode configure(HttpSecurity).
 * L'idée de cette classe est de dire à SpringSecurity comment hasher les mots de passe,
 * comment récupérer des User (ici, depuis la base de données) et quelles routes seront
 * accessibles à qui
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserRepository repo;

    /**
     * Cette méthode est celle qui sera le plus amener à changer, car c'est dans celle
     * ci qu'on va dire grâce à des mvcMatcher quelle page est accessible à quel rôle,
     * quel page est public.
     * Ici le site est configuré pour avoir certaines pages indiquées protégées et toutes
     * les autres public accessibles à toustes
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        //Ici, rajouter des mvcMatcher("/route-a-proteger").authenticated() pour protéger des pages
        .mvcMatchers("/account/**").authenticated()
        .mvcMatchers("/admin/**").hasRole("ADMIN")
        .anyRequest().permitAll()
        .and().formLogin();
    
    }

    /**
     * Cette méthode permet de configurer SpringSecurity pour lui dire quelle classe lui
     * permettra de récupérer les user. Ici on lui donne notre UserRepository car on lui
     * a fait implémenter l'interface UserDetailsService (c'est obligatoire pour que 
     * Spring puisse s'en servir)
     * On lui dit également quelle classe de Hashing de mot de passe on utilise, ici,
     * on lui donne une instance de Bcrypt qui est un des algorithmes parmis les plus
     * utilisés actuellement (01/2022)
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(repo).passwordEncoder(passwordEncoder());
    }

    /**
     * Ici on crée un Bean qui nous permettra de @Autowired ce PasswordEncoder dans
     * n'importe quel autre classe (par exemple notre contrôleur où on fera l'inscription)
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

}
